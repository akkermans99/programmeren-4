const http = require('http');
const database = require('./src/database');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  
  let movie = {
      title: "Spiderman:homecoming",
      duration: 211,
      genres: [
          "action",
          "adventure"
      ],
      adult: false
  };
  database.movies.push(movie);
  res.end(JSON.stringify(database));
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});